import 'package:flutter/material.dart';
import 'package:flutter_http_task/core/config/dark_theme_config.dart';
import 'package:flutter_http_task/core/config/light_theme_config.dart';
import 'package:flutter_http_task/screens/auth/login.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> navState = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(builder: (context, orientation, screenType) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'FlutterHttp',
        theme: LightThemeConfig.themeData ,
        themeMode: ThemeMode.light,
        darkTheme: DarkThemeConfig.themeData,
        home: SignInScreen(),
      );
    });
  }
}

