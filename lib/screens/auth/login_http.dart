import 'package:flutter/cupertino.dart';
import 'package:flutter_http_task/core/models/network_request_model.dart';
import 'package:flutter_http_task/core/services/notification_service.dart';

class HandleLogin {
  NetworkRequest loginRequest = NetworkRequest(endpoint: '/login', showLoader: true);

  Future<dynamic> login (Map data) async{
    // BuildContext? dialog = await NotificationService.showLoader(message: 'loading...');
    await loginRequest.post(data);
    // NotificationService.closeLoader(dialog);
    return loginRequest.response;
  }
}