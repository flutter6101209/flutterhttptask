import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_http_task/core/components/button.dart';
import 'package:flutter_http_task/core/components/password_text_field.dart';
import 'package:flutter_http_task/screens/auth/login_http.dart';
import 'package:flutter_http_task/core/services/network_service.dart';
import 'package:flutter_http_task/core/services/validation-service.dart';

import '../../core/components/custom_field_text.dart';
import '../../core/models/network_request_model.dart';
import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

class SignInScreen extends StatelessWidget {
  SignInScreen({Key? key}) : super(key: key);
  TextEditingController emailController =
      TextEditingController(text: 'aladesiuntope@gmail.com');
  TextEditingController passwordController =
      TextEditingController(text: 'tope123');

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Welcome back',
              textAlign: TextAlign.start,
            ),
            SizedBox(
              height: 20,
            ),
            CustomTextField(
              label: 'Email',
              hintText: 'your mail',
              controller: emailController,
            ),
            SizedBox(
              height: 20,
            ),
            PasswordTextFormField(
              validator: (String? v) => ValidationService().isValidInput(v),
              label: 'Enter your password',
              hintText: '*****',
              controller: passwordController,
            ),
            SizedBox(
              height: 20,
            ),
            PrimaryButton(
                title: 'Logn',
                onPressed: () async {
                  Map response = await HandleLogin().login({
                    'password': passwordController.text,
                    'email': emailController.text
                  });
                  print(
                    response
                  );
                })
          ],
        ),
      ),
    ));
  }
}
