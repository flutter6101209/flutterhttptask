import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class BrandStyleConfig {
  //COLORS
  static const Color lightPrimary = Color(0xfff2e2ec);
  static const Color primary = Color(0xff8D005F);
  static final Color offPrimary = const Color(0xff8D005F).withOpacity(0.15);

  static const Color secondary = Color(0xff416AC4);
  static const Color offSecondary = Color(0xffC1DBF4);

  //COLOR SCHEME
  static  ColorScheme colorScheme =  ColorScheme.light(
    primary: Colors.white,
    surface: Color(0xff8D005F),
    secondary: Colors.black,
    tertiary: Color(0xffebebeb),
    surfaceTint: Color(0xff8D005F).withOpacity(0.05),
    primaryContainer: Colors.white,
    onPrimaryContainer: Color(0xff4e4e4e),
    onSurface: Color(0xfff2e2ec),
    tertiaryContainer: Color(0xffecf0f2),
    background: Color(0xfff4edf1),
    surfaceVariant: Color(0xfff9f2f7),
    inversePrimary: Color(0xff222222),
    onBackground: Color(0xfff0f0f0),
    onTertiary: Color(0xff222222),
    onSurfaceVariant: Color(0xff7c7c7c),
    //  surfaceTint: Color(0xfff2e2ec),
    onTertiaryContainer: Color(0xffeaf1f6),
    secondaryContainer: Color(0xfff5eaf3),

  );

  static const ColorScheme darkColorScheme =  ColorScheme.dark(
    primary: Colors.black,
    surface: Colors.white,
    secondary: Color(0xffb9b9b9),
    tertiary: Color(0xff383838),
    surfaceTint: Color(0xff2a292a),
    primaryContainer: Color(0xff2a292a),
    onPrimaryContainer: Color(0xffd5d5d5),
    onSurface: Color(0xff2a292a),
    tertiaryContainer: Color(0xff0d2a40),
    background: Color(0xff520c3b),
    surfaceVariant: Color(0xff393939),
    inversePrimary: Color(0xffa3a3a3),
    onBackground: Color(0xff2f2f2f),
    onTertiary: Color(0xffffffff),
    onSurfaceVariant: Color(0xff979797),
    onTertiaryContainer: Color(0xff0b3d57),
    secondaryContainer: Color(0xff590d48),
  );

  //Elevated Text Theme
  static final TextTheme textTheme = TextTheme(
    //HEADLINE
    displayLarge: TextStyle(fontSize: 57.sp),
    displayMedium: TextStyle(fontSize: 45.sp),
    displaySmall: TextStyle(fontSize: 36.sp),
    headlineLarge: TextStyle(fontSize: 32.sp),
    headlineMedium: TextStyle(fontSize: 28.sp),
    headlineSmall: TextStyle(fontSize: 24.sp),

    //BODY
    bodyLarge: TextStyle(fontSize: 18.sp),
    bodyMedium: TextStyle(fontSize: 16.sp),
    bodySmall: TextStyle(fontSize: 14.sp),

    //LABEL
    labelLarge: TextStyle(fontSize: 15.sp),
    labelMedium: TextStyle(fontSize: 12.sp),
    labelSmall: TextStyle(fontSize: 11.sp),

    //TITLE
    titleLarge: TextStyle(fontSize: 22.sp),
    titleMedium: TextStyle(fontSize: 16.sp),
    titleSmall: TextStyle(fontSize: 14.sp),

    //custom size
  );

  //Elevated Button Theme
  static final ElevatedButtonThemeData elevatedButtonThemeData =
  ElevatedButtonThemeData(
      style: ButtonStyle(
        elevation: const MaterialStatePropertyAll<double>(1),
        textStyle: MaterialStatePropertyAll<TextStyle>(
            textTheme.bodyMedium!.copyWith(fontWeight: FontWeight.w500)),
        minimumSize: const MaterialStatePropertyAll<Size>(Size(100, 45)),
        backgroundColor: const MaterialStatePropertyAll<Color>(primary),
      ));

  //CheckBox Theme
  static const CheckboxThemeData checkboxThemeData =
  CheckboxThemeData(fillColor: MaterialStatePropertyAll<Color>(primary));

  //RadioButton Theme
  static const RadioThemeData radioThemeData =
  RadioThemeData(fillColor: MaterialStatePropertyAll<Color>(primary));

  //InputDecoration Theme
  static final InputDecorationTheme inputDecorationTheme = InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5),
        borderSide: const BorderSide(color: Colors.black12)),
    focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(5),
        borderSide: BorderSide(color: offPrimary)),
  );
}
