import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'brand_config.dart';
import 'light_theme_config.dart';

class DarkThemeConfig {

  static final ThemeData themeData = LightThemeConfig.themeData.copyWith(
    backgroundColor: Colors.black,
    primaryColor: BrandStyleConfig.primary,
    scaffoldBackgroundColor: Colors.black,
    colorScheme: BrandStyleConfig.darkColorScheme,
    primaryColorLight: Colors.white,
    brightness: Brightness.dark,
    textTheme: BrandStyleConfig.textTheme.apply(displayColor: Colors.white, bodyColor: Colors.white),
    canvasColor: Color(0xff2f2f2f),
    appBarTheme: AppBarTheme(
        color: Colors.black,
        elevation: 0)
    );

}