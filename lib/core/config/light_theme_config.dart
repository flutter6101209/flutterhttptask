import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'brand_config.dart';

class LightThemeConfig {


  static final ThemeData themeData = ThemeData(
    fontFamily: "SF UI Display",
    colorScheme: BrandStyleConfig.colorScheme,
    backgroundColor: Colors.white,
    primaryColor: BrandStyleConfig.primary,
    scaffoldBackgroundColor: Color(0xfffafafa),
    textTheme: BrandStyleConfig.textTheme.apply(displayColor: Colors.black, bodyColor: Colors.black),
    elevatedButtonTheme: BrandStyleConfig.elevatedButtonThemeData,
    checkboxTheme: BrandStyleConfig.checkboxThemeData,
    radioTheme: BrandStyleConfig.radioThemeData,
    inputDecorationTheme: BrandStyleConfig.inputDecorationTheme,
    primaryColorDark: Colors.black,
    brightness: Brightness.light,
    canvasColor: Color(0xfff1f1f1),
    appBarTheme: AppBarTheme(
      color: BrandStyleConfig.colorScheme.primary,
      elevation: 0)
  );
}