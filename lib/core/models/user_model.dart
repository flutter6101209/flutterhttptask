
class UserModel {
  int customerId;
  String customerFirstName;
  String customerLastName;
  String customerBusinessName;
  String customerAccountType;
  String customerEmail;
  String customerPhone;
  String customerCountry;
  String customerCountryCode;
  String customerOnlineStatus;
  String customerStatus;
  String customerCreatedAt;
  String customerUpdatedAt;
  String customerLastLoginAt;
  String token;
  List organisations;

  UserModel({
    required this.customerId,
    required this.customerFirstName,
    required this.customerLastName,
    required this.customerBusinessName,
    required this.customerAccountType,
    required this.customerEmail,
    required this.customerPhone,
    required this.customerCountry,
    required this.customerCountryCode,
    required this.customerOnlineStatus,
    required this.customerStatus,
    required this.customerCreatedAt,
    required this.customerUpdatedAt,
    required this.customerLastLoginAt,
    required this.token,
    required this.organisations,
  });

  factory UserModel.fromJson(Map data){
    return UserModel(
        customerId: data['customerId'],
        customerFirstName: data['customerFirstName'],
        customerLastName: data['customerLastName'],
        customerBusinessName: data['customerBusinessName'],
        customerAccountType: data['customerAccountType'],
        customerEmail: data['customerEmail'],
        customerPhone: data['customerPhone'],
        customerCountry: data['customerCountry'],
        customerCountryCode: data['customerCountryCode'],
        customerOnlineStatus: data['customerOnlineStatus'],
        customerStatus: data['customerStatus'],
        customerCreatedAt: data['customerCreatedAt'],
        customerUpdatedAt: data['customerUpdatedAt'],
        customerLastLoginAt: data['customerLastLoginAt'],
        token: data['token'],
        organisations: data['organisations']
    );
  }

}