import 'dart:convert';
import 'package:http/http.dart' as http;

class NetworkResponse<T> {
  int? statusCode;
  String? responseCode;
  T? data;
  String? message;


  NetworkResponse(http.Response response){
    statusCode= response.statusCode;
    data = jsonDecode(response.body);
    if(data is Map){
      Map dt = data as Map;
      if(dt.containsKey('responseCode')){
        responseCode= dt['responseCode'];
        message = dt['responseMessage'];
      }
    }
  }

  @override
  String toString() {
    return "${statusCode}: ${message} => ${data}";
  }

}