import 'dart:async';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../services/network_service.dart';
import '../services/notification_service.dart';
import 'network_response_model.dart';

enum NetworkRequestStatus {
  idle, pending, error, completed
}

class NetworkRequest<T> {
  NetworkResponse? response;

  final statusController = BehaviorSubject<NetworkRequestStatus>();
  NetworkRequestStatus get status => statusController.value;

  String endpoint;
  bool showLoader;
  bool showError;
  String loaderTxt;
  BuildContext? dialogContext;

  NetworkRequest({
    required this.endpoint,
    this.showLoader = false,
    this.showError = true,
    this.loaderTxt= "Please wait..."}){
   statusController.add(NetworkRequestStatus.idle);
  }

  Future<void> post(Map payload)async {
    if(showLoader){
      dialogContext = await NotificationService.showLoader(message: loaderTxt);
    }

    statusController.add(NetworkRequestStatus.pending);
    response = await NetworkService().post(endpoint, payload);
    _handleError(response!);

    if(showLoader){
      NotificationService.closeLoader(dialogContext);
    }
  }

  void _handleError(NetworkResponse response){
    if(!NetworkService.successResponseCodes.contains(response.statusCode)){
      statusController.add(NetworkRequestStatus.error);
      if(showError) {
        NotificationService.showError(response.message!);
      }
      return;
    }
    statusController.add(NetworkRequestStatus.completed);
  }

}