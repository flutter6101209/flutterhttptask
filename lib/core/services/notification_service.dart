import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import 'navigation_service.dart';

class NotificationService {

  static void showError(String message){
    showTopSnackBar(
      NavigationService.navState.currentState!.overlay!,
      CustomSnackBar.error(
        message: message,
      ),
    );
  }

  static void showSuccess(String message){
    showTopSnackBar(
      NavigationService.navState.currentState!.overlay!,
      CustomSnackBar.success(
        message: message,
      ),
    );
  }

  static Future<BuildContext?> showLoader({String? message})async {
    BuildContext? dialogContext;
    showDialog(
      context: NavigationService.navState.currentContext!,
      barrierColor: Colors.black.withOpacity(.8),
      barrierDismissible: false,
      builder: (BuildContext context) {
        dialogContext = context;
        return SimpleDialog(
          contentPadding: const EdgeInsets.symmetric(vertical: 20),
          children: [
            Text(
              message ?? "Please wait...",
              style: const TextStyle(fontSize: 15),
              textAlign: TextAlign.center)
          ],
        );
      },
    );

    await Future.delayed(const Duration(milliseconds: 500));
    return dialogContext;
  }

  static void closeLoader(BuildContext? context){
    print("Context");
    print(context);
    if(context != null){
      Navigator.of(context, rootNavigator: true).pop();
    }
  }
}