import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../models/network_response_model.dart';

abstract class INetworkService {
  Future<NetworkResponse> get(String endpoint);
  Future<NetworkResponse> post(String endpoint, Map payload);
}

class NetworkService extends INetworkService {
  final String _baseURL = "https://controller.kaffir.ng/api";
  static List<int> successResponseCodes = [200, 201, 210];

  @override
  Future<NetworkResponse> get(String endpoint) async {
    Uri uri = Uri.parse(_baseURL+endpoint);

    if (kDebugMode) {
      print("Get: => ${uri.toString()}");
    }

    http.Response response = await http.get(uri);
    NetworkResponse<Map> networkResponse = NetworkResponse(response);

    return Future.value(networkResponse);
  }

  @override
  Future<NetworkResponse<Map>> post(String endpoint, Map payload) async{
    Uri uri = Uri.parse(_baseURL+endpoint);

    if (kDebugMode) {
      print("Post: ${payload} => ${uri.toString()}");
    }

    http.Response response = await http.post(uri, body: jsonEncode(payload), headers: {
      "content-type": "application/json",

    }).timeout(const Duration(minutes: 1));

    NetworkResponse<Map> networkResponse = NetworkResponse(response);

    if (kDebugMode) {
      print(networkResponse.toString());
    }

    return Future.value(networkResponse);
  }

}