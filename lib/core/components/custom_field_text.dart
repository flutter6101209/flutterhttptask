
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../config/brand_config.dart';

class CustomTextField extends StatelessWidget {
  final String label;
  final String hintText;
  TextEditingController? controller;
  final String? Function(String?)? validator;

  CustomTextField({Key? key,
    required this.label,
    required this.hintText,
    this.controller,
    this.validator
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label,
            style: Theme.of(context).textTheme.labelLarge?.copyWith(
                fontWeight: FontWeight.w300
            )
        ),
        const SizedBox(height: 10),
        TextFormField(
          controller: controller,
          decoration:  InputDecoration(
              contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
              border:   const OutlineInputBorder(),
              hintText: hintText,
              hintStyle:  TextStyle(
                  fontSize: 14.5.sp,
                  fontWeight: FontWeight.w300,
                  color: Theme.of(context).colorScheme.onSurfaceVariant
              ),
              fillColor: Theme.of(context).colorScheme.tertiary,
              filled: true,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide:  BorderSide(
                      color:Theme.of(context).colorScheme.tertiary,
                      width: 1.0
                  )
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: const BorderSide(
                    color: BrandStyleConfig.primary,
                    width: 1.0
                ),
              )
          ),
          validator: validator,
          cursorColor: Theme.of(context).primaryColor,
        ),
      ],
    );
  }
}