import 'package:flutter/material.dart';

import '../config/brand_config.dart';

class PasswordTextFormField extends StatefulWidget {
  final String label;
  final String hintText;
  final TextEditingController? controller;
  final String? Function(String?)? validator;

  const PasswordTextFormField(
      {Key? key,
        required this.label,
        required this.hintText,
        this.validator,
        this.controller
      })
      : super(key: key);

  @override
  State<PasswordTextFormField> createState() => _PasswordTextFormFieldState();
}

class _PasswordTextFormFieldState extends State<PasswordTextFormField> {
  bool _obscureText = true;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.label,
            style: Theme.of(context).textTheme.labelLarge?.copyWith(fontWeight: FontWeight.w300)
        ),
        const SizedBox(height: 10),
        TextFormField(
          controller: widget.controller,
          obscureText: _obscureText,
          decoration: InputDecoration(
              suffixIcon: TextButton(
                child:  Text(
                  _obscureText ? 'Show' : 'Hide',
                  style:TextStyle(color: Theme.of(context).colorScheme.surface,),
                ),
                onPressed: () {
                  setState(() {
                    _toggle();
                  });
                },
              ),
              border: OutlineInputBorder(),
              hintText: widget.hintText,
              hintStyle: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                  color: Theme.of(context).colorScheme.onSurfaceVariant),
              fillColor: Theme.of(context).colorScheme.tertiary,
              filled: true,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide(
                      color: Theme.of(context).colorScheme.tertiary,
                      width: 1.0)),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: const BorderSide(
                    color: BrandStyleConfig.primary, width: 1.0),
              )),
          validator: widget.validator,
          cursorColor: Theme.of(context).primaryColor,
        ),
      ],
    );
  }
}
