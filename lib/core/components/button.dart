import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final String title;
  final double? height;
  final double? width;
  final Function()? onPressed;
  final bool disabled;
  final bool isLoading;

  const PrimaryButton({
    Key? key,
    required this.title,
    required this.onPressed,
    this.height,
    this.width,
    this.disabled = false,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bgColor = _getColor(context);
    return SizedBox(
      height: height ?? 48.0,
      width: width ?? double.infinity,
      child: ElevatedButton(
        onPressed: disabled || isLoading ? null : onPressed,
        style: ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(bgColor)
        ),
        child: Text(
          title,
          style: const TextStyle(
              color: Colors.white,
              fontSize: 15,
              fontWeight: FontWeight.w400
          ),
        ),
      ),
    );
  }

  Color _getColor(BuildContext context){
    return disabled ? Colors.grey
        : isLoading ? Theme.of(context).primaryColor.withOpacity(0.6)
        : Theme.of(context).primaryColor;
  }
}
